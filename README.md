Informatica@economia.unife
==========================

Questa repository contiene il materiale dell'insegnamento di Informatica del corso di Economia.

A partire da Source per ogni settimana del corso e' possibile recuperare le slide di lezione. 

Tra i materiali sono anche presenti:

- esercizi: il file conterra' il nome esercizio ed estensione .txt)
- possibili domande.
